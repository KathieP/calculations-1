# Setup intstructions
--1) Create a parameter called 'Date Comparison' and show parameter control
--2) Create a calculated field called: 'Current Date Period'

<code>
IF [Date Comparison] = "Current vs. Prior Month"
  OR [Date Comparison] = "Current Month YoY"
  OR [Date Comparison] = "Current MTD vs. Prior MTD"
  THEN
    DATEDIFF('month',[Order Date],[As of Date]) = 0
ELSEIF [Date Comparison] = "Current vs. Prior Quarter"
  OR [Date Comparison] = "Current Quarter YoY"
  OR [Date Comparison] = "Current QTD vs. Prior QTD"
  THEN
    DATEDIFF('quarter',[Order Date],[As of Date]) = 0
ELSEIF [Date Comparison] = "Current vs. Prior Year"
  OR [Date Comparison] = "Current YTD vs. Prior YTD"
  THEN
    DATEDIFF('year',[Order Date],[As of Date]) = 0
ELSEIF [Date Comparison] = "Rolling 12 Months"
  THEN
    DATEDIFF('month',[Order Date],[As of Date]) < 12
      AND [Order Date] <= [As of Date]
ELSEIF [Date Comparison] = "Rolling 30 Days"
  THEN
    DATEDIFF('day',[Order Date],[As of Date]) < 30
      AND [Order Date] <= [As of Date]
END
</code>
